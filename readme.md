# Holiday route planner

## Input
The expected format: "x => z".
- The "=>" characters are always needed even if there are nothing after it. If the character is not given or 
any other sign is given for input it will raise an exception
- The spaces before and after are optionals.
- One or more english character letters can be used for defining a route
- If there is a cycle in the input it will raise an error (cycle is: "a=>b and b=>a")
- Trailing new lines and tab characters will raise error
- If a right side route is not present in a left side it will raise an error

## How to run it
The program has a public "calculate" method that will expect a filename that is placed in the same folder.
The file should only contain the defined input above. 
Output is a sored list of routes

## Requirements
- Python 3.7
- No external dependencies