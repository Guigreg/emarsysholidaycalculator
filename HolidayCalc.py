import re

SEPARATOR_MARK = '=>'
REGEX = r'([a-z]+)\s?=>\s?([a-z]*)'


def __split_by_separator(line, separator):
    element = line.replace(" ", "").split(separator)
    return list(filter(None, element))


def __process_sequence(routes, route_dependencies):
    for e in routes:
        after = route_dependencies.get(e)
        if after is not None:
            __move_if_needed(e, after, routes)


def __move_if_needed(before, after, routes):
    bi = routes.index(before)
    ai = routes.index(after)
    if bi > ai:
        routes.remove(before)
        routes.insert(ai, before)


def __validate_input(routes, route_dependencies):
    for from_route, to_route in route_dependencies.items():
        if from_route not in routes:
            raise Exception('Bad input! ', from_route, ' is not in the route list!')
        if to_route not in routes:
            raise Exception('Bad input! ', to_route, ' is not in the route list!')


def __is_cyclic(route_dependencies):
    path = set()

    def visit(element):
        path.add(element)
        for neighbour in route_dependencies.get(element, ()):
            if neighbour in path or visit(neighbour):
                return True
        path.remove(element)
        return False

    return any(visit(v) for v in route_dependencies)


def calculate(file_name):
    with open(file_name, 'r') as file:
        lines = file.read().split('\n')

        route_dependencies = dict()
        routes = list()

        for line in lines:
            found = re.findall(REGEX, line)
            if not found:
                raise Exception('Bad input detected in line: ', line)
            if found[0][1]:
                route_dependencies[found[0][1]] = found[0][0]
            routes.append(found[0][0])

        __validate_input(routes, route_dependencies)
        if __is_cyclic(route_dependencies):
            raise Exception('Cycle detected! Terminating...')

        __process_sequence(routes, route_dependencies)
    return routes
