import unittest

from HolidayCalc import calculate


class HolidayCalcTest(unittest.TestCase):
    def test_small_cycle(self):
        with self.assertRaises(Exception):
            calculate('smallCycle.txt')

    def test_bigger_cycle(self):
        with self.assertRaises(Exception):
            calculate('biggerCycle.txt')

    def test_with_one_length_cycle(self):
        with self.assertRaises(Exception):
            calculate('oneLengthCycle.txt')

    def test_with_dependencies(self):
        self.assertEqual(calculate('fileWithDependencies1.txt'), ['u', 'z', 'w', 'v'])

    def test_with_dependencies_big(self):
        self.assertEqual(calculate('fileWithDependencies2.txt'), ['u', 'z', 'w', 'v', 'x', 'y'])

    def test_with_no_dependencies(self):
        self.assertEqual(calculate('noDependencies.txt'), ['a', 'b', 'c', 'd', 'q'])

    def test(self):
        self.assertEqual(calculate('moreLetter.txt'), ['abc', 'xyz'])

    def test_with_bad_character(self):
        with self.assertRaises(Exception):
            calculate('badInput.txt')

    def test_with_not_existing_right_route(self):
        with self.assertRaises(Exception):
            calculate('badInput2.txt')

    def test_with_trailing_new_lines(self):
        with self.assertRaises(Exception):
            calculate('badInput3.txt')


if __name__ == '__main__':
    unittest.main()
